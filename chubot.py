#It's to test bot stuff, ahoy
import discord
from discord.ext.commands import Bot
from discord.ext import commands
import random
import asyncio
import pickle
import os
import sec
import gie

#Varstuff
token = sec.token()

Client = discord.Client()
client = commands.Bot(command_prefix = "?")

tphId = "000"

adId = "000"
#adId = "000" #TEServer

#fedeId = "000" #TEServer
fedeId = "000"

#fedrId = "000" #TEServer
fedrId = "000"

bover = "0.?"

orat = "a"
orvt = "a"
urg = False
urt = False

profis = []
meals = []

#Classtuff
class Uprof():
    def __init__(self, name, uid):
        self.atName = name
        self.wrn = "noname"
        self.uid = uid
        self.utype = "both"
        self.wg = 1 #Kg
        self.sz = 1 #Meters
        self.dwg = 0
        self.dsz = 0
        self.fnu = 0
        profis.append(self)

class Meal():
    def __init__(self, name, wMin, wMax, sMul):
        self.name = name
        self.wmin = wMin
        self.wmax = wMax
        self.smul = sMul
        meals.append(self)

ml0 = Meal('small', 1, 25, 1)

#moredefstuff
def obGen(thena, theu):
    exec("up%i = Uprof('%s', '%s')" %(len(profis),thena, theu))

def meGen(n, wn, wx, sm):
    exec("ml%i = Meal('%s', %i, %i, %i)" %(len(meals) , n, int(wn), int(wx), int(sm)))


def geSize(wgIn, sm):
    toret = 0

    toret = random.randrange(int(wgIn-wgIn/2), int(wgIn+wgIn/3))
    vtoret = int(float(toret)**(2/3)+(((toret/2)+sm)))
    return vtoret

#defPic
def picS():
    global profis
    global meals
    topic = [profis, meals]
    pickle_out = open("ct.pickle","wb")
    pickle.dump(topic,pickle_out)
    pickle_out.close()
    gie.pibg()

def picL():
    global profis
    global meals
    gie.pibn()
    pickle_in = open("ct.pickle","rb")
    toupic = pickle.load(pickle_in)
    profis = toupic[0]
    meals = toupic[1]
    pickle_in.close()

#Messages
lookWS = ["almost destroyed a scale but discovered they weight", "needed an industrial scale, but now they know that they weight", "deducted that they weight about"]
lookSZ = ["and just by looking at them, you know that they measure about", "and with some help of an extra long measure tape, they also know that they have a lovely size of"]
feed1 = ["gave to", "made to", "had the idea to give","gifted","just felt like enticing","were pretty much up to feed"]
feed2 = ["a lovely", "an incredible","to gorge themselves in, a", "to feast themselves with, a", "the"]

#Runstuff
@client.event
async def on_ready():
    print("[O]Bot Ready")
    try:
        picL()
        print("[O]Everything loaded")
    except FileNotFoundError:
        print("[X]File 'ct.pickle' wasn't created, use 'chu-ver' on the server to create one.")
        print("[ ]Or just copy 'chubotDB.pickle'<deprecated> to 'ct.pickle' to have the default one.")

async def register(me, ar):
    message = me
    args = ar
    its = message.author.mention.replace("!","")

    print("%s, %s" %(message.author,args))
    if len(args) == 3:
        if fedeId in [role.id for role in message.author.roles] and fedrId in [role.id for role in message.author.roles]:
            try:
                print('TryB')
                obGen(its, message.author.id)
                profis[len(profis)-1].utype = "both"
                profis[len(profis)-1].wrn = str(message.author.name)
                profis[len(profis)-1].wg = int(args[1])
                profis[len(profis)-1].dwg = int(args[1])
                profis[len(profis)-1].sz = int(args[2])
                profis[len(profis)-1].dsz = int(args[2])
                picS()
                #await client.send_message(message.channel, "Allo")
                await client.send_message(message.channel, "%s registered as both feeder and feedee with the weight and waist size of %ikg and %icm" %(profis[len(profis)-1].atName, profis[len(profis)-1].wg, profis[len(profis)-1].sz))
                #print(profis)
            except:
                await client.send_message(message.channel, "You seem to have typed something wrong, make sure to only use whole numbers when registering weight/size.")
                profis.pop(len(profis)-1)
                #print(profis)
        elif fedeId in [role.id for role in message.author.roles]:
            try:
                print('try ee')
                obGen(its, message.author.id)
                profis[len(profis)-1].utype = "feedee"
                profis[len(profis)-1].wrn = str(message.author.name)
                profis[len(profis)-1].wg = int(args[1])
                profis[len(profis)-1].dwg = int(args[1])
                profis[len(profis)-1].sz = int(args[2])
                profis[len(profis)-1].dsz = int(args[2])
                picS()
                await client.send_message(message.channel, "%s registered as a feedee with the weight and waist size of %skg and %scm, respectively" %(profis[len(profis)-1].atName,profis[len(profis)-1].wg, profis[len(profis)-1].sz))
                #print(profis)
            except:
                await client.send_message(message.channel, "You seem to have typed something wrong, make sure to only use whole numbers when registering weight/size.")
                profis.pop(len(profis)-1)
                #print(profis)
        elif fedeId not in [role.id for role in message.author.roles] and fedrId not in [role.id for role in message.author.roles]:
            await client.send_message(message.channel, "%s doesn't have any of the necessary roles to register" %(message.author.mention))
    elif len(args) == 1:
        if fedrId in [role.id for role in message.author.roles]:
            print('try r')
            obGen(its, message.author.id)
            profis[len(profis)-1].utype = "feeder"
            profis[len(profis)-1].wrn = str(message.author.name)
            #print(profis)
            picS()
            await client.send_message(message.channel, "%s registered as a feeder" %(profis[len(profis)-1].atName))
        elif fedeId not in [role.id for role in message.author.roles] and fedrId not in [role.id for role in message.author.roles]:
            await client.send_message(message.channel, "%s doesn't have any of the necessary roles to register" %(message.author.mention))
    elif len(args) != 2 and len(args) != 3:
        await client.send_message(message.channel, "You seem to have wrongly typed this command, use 'chu-register <weight> <size>'")

async def sayT(mess,inpt):
    message = mess
    await client.send_message(message.channel,inpt)

@client.event
async def on_message(message):
    global urg
    global urt
    global orat
    global orvt
    if message.content.lower().startswith("chu-register"): #use: chu-register <weight in kg> <size in m>
        regob = 0
        args = message.content.split(" ")
        print(args)
        if len(profis) > 0:
            for i in profis:
                if message.author.id in i.uid:
                    regob = 5
                    print('r5')
                    #await client.send_message(message.channel, "You are already registered")
                elif message.author.id not in i.uid:
                    print('r01')
                    regob = 0
                    #await register(message, args)
        elif len(profis) == 0:
            print('r02')
            regob = 0
            #await register(message, args)
        print(regob)
        if regob == 0:
            print('eh')
            await register(message, args)
            regob = 0
        elif regob == 5:
            print('oh')
            await client.send_message(message.channel, "You are already registered.")
            regob = 0

    if message.content.lower().startswith("chu-unregister"): #use: chu-unregister
        orat = message.author.id
        urg = True
        await client.send_message(message.channel, "Are you sure %s? Unregistering will wipe any status you have, say '-yes' to unregister or say '-no' to cancel" %(message.author.mention))
    if message.content.lower().startswith("-yes") and urg == True:
        trs = 5
        if message.author.id == orat:
            for i in profis:
                if orat in i.uid:
                    profis.pop(profis.index(i))
                    trs += 5
            if trs > 5:
                picS()
                await client.send_message(message.channel, "%s succefully unregistered from chuBot" %(message.author.mention))
                urg = False
                orat = "a"
                trs = 5
            elif trs < 9:
                await client.send_message(message.channel, "Your profile wasn't deleted, are you sure you have a profile or requested removal?")
                trs = 5
                urg = False
                orat = "a"
        elif message.author.id != orat:
            await client.send_message(message.channel, "You %s don't seem to be the one that requested to unregister, please don't mess with other people's profiles" %(message.author.mention))
    if message.content.lower().startswith("-no") and urg == True:
        if message.author.id == orat:
            orat = "a"
            urg = False
            await client.send_message(message.channel, "You cancelled the deletion, %s!" %(message.author.mention))
        elif message.author.id != orat:
            await client.send_message(message.channel, "You %s don't seem to be the one that requested to unregister, please don't mess with other people's profiles" %(message.author.mention))
    if message.content.lower().startswith("-me"): #use: -me
        for i in profis:
            if message.author.id in i.uid:
                await client.send_message(message.channel, "%s %s %ikg %s %icm wide" %(i.wrn, random.choice(lookWS),i.wg, random.choice(lookSZ),i.sz))
    if message.content.lower().startswith("-feed"): #use chu-feed <@> <meal>
        #print("aa")
        arg = message.content.split(" ")
        #print(arg)
        for t in arg:
            if t == " ":
                arg.pop(arg.index(t))
        #print(arg)
        if len(arg) == 3:
            arg.pop(0)
            acee = ["feedee", "both"]
            acer = ["feeder","both"]
            ptf = "a"
            pff = "b"
            kgp = 0
            szp = 0
            chm = "a"
            goto1 = 0
            goto2 = 0
            itff = arg[0].replace("!","")
            fdd = 0

            for i in meals:
                if arg[1].lower() == i.name.lower():
                    kgp = random.randrange(i.wmin, i.wmax+1)
                    chm = i
                    fdd = 1

            szp = int(geSize(kgp, chm.smul))

            for g in profis:
                #print('arg0',itff)
                #print(g.atName)
                if itff in g.atName:
                    ptf = g
                    #print("ptf:",ptf.wrn)
                    goto1 = 1
            for i in profis:
                if message.author.id in i.uid:
                    pff = i
                    #print("pff:",pff.wrn)
                    goto2 = 1

            print(goto1,goto2)
            if goto1 == 1 and goto2 ==1 and fdd == 1 and pff.utype in acer and ptf.utype in acee:
                print('fed')
                pff.fnu += 1
                ptf.wg += kgp
                ptf.sz += szp
                picS()
                await client.send_message(message.channel, "%s %s %s %s %s meal. Now %s weights %ikg and has the size of %scm" %(pff.wrn, random.choice(feed1),ptf.wrn, random.choice(feed2), arg[1], ptf.wrn, ptf.wg,ptf.sz))
                goto1 = 0
                goto2 = 0
                fdd = 0
            elif pff not in profis:
                await client.send_message(message.channel, "Sorry, but you couldn't feed them, you aren't registered.")
            elif ptf not in profis:
                await client.send_message(message.channel, "Sorry, but you couldn't feed them, they aren't registered.")
            elif pff.utype not in acer:
                await client.send_message(message.channel, "Sorry, but you couldn't feed them, you don't have the role to feed.")
            elif ptf.utype not in acee:
                await client.send_message(message.channel, "Sorry, but you couldn't feed them, they don't have the role to be fed.")
            elif fdd != 1:
                await client.send_message(message.channel, "Sorry, but you couldn't feed them, the meal you said isn't valid.")
            else:
                print("wrrr")
        elif len(arg) != 3:
            await client.send_message(message.channel, "Sorry? seems you wrongly typed a command, use '-feed <@feedee> <meal>'")
    if message.content.lower().startswith('chu-rank'): #use chu-rank
        genb = 1
        ferlist = profis
        ferlist.sort(key=lambda x: x.fnu, reverse=True)
        fedlist = profis
        fedlist.sort(key=lambda x: x.wg, reverse=True)
        for h in profis:
            print("%s %s %s" %(h.atName, h.wrn, h.uid))

        await client.send_message(message.channel, "---**Feedees**---")
        for i in fedlist:
            if i.utype == "feedee" or i.utype == "both":
                await client.send_message(message.channel, "# %s | %ikg | %icm" %(i.wrn,i.wg,i.sz))
                genb +=1
        await client.send_message(message.channel, "---**Feeders**---")
        genb = 1
        for i in ferlist:
            if i.utype == "feeder" or i.utype == "both":
                await client.send_message(message.channel, "# %s | has fed %i times" %(genb,i.wrn,i.fnu))
                genb += 1
        await client.send_message(message.channel, "---------------")
    if message.content.lower().startswith('shu-sw'): #Use shu-sw @ w s f
        args = message.content.split(" ")
        args.pop(0)
        itff = arg[0].replace("!","")
        if adId in [role.id for role in message.author.roles] or tphId in [role.id for role in message.author.roles]:
            if len(args) == 3:
                for i in profis:
                    if itff in i.atName:
                        i.wg = int(args[1])
                        i.sz = int(args[2])
                        i.fnu = int(args[3])
                        picS()
                        await client.send_message(message.channel, "If the person you selected has a profile, their weight and size are now what you set.")
            if len(args) != 3:
                await client.send_message(message.channel, "This command was wrongly typed, use 'chu-sw <@person> <wg in kg> <sz in cm>'")
        elif adId not in [role.id for role in message.author.roles]:
            await client.send_message(message.channel, "You don't have the permisson to use this command, only moderators/admins can use it.")
    if message.content.lower().startswith('shu-rm'): #Use shu-rm <@> -y
        args = message.content.split(" ")
        if adId in [role.id for role in message.author.roles] or tphId in [role.id for role in message.author.roles]:
            if len(args) == 3 and args[2] == "-y":
                for i in profis:
                    if args[1] in i.atName:
                        profis.pop(profis.index(i))
                        picS()
                        await client.send_message(message.channel, "If the person you selected had a profile, their profile is now removed")
            elif len(args) != 3:
                await client.send_message(message.channel, "This command was wrongly typed, use 'chu-rm <@person> -y', WARNING: This command won't ask you for comfirmation before removing the profile!")
        elif adId not in [role.id for role in message.author]:
            await client.send_message(message.channel, "You don't have the permission to use this command, only moderators/admins can use it.")
    if message.content.lower().startswith('shu-mkmeal'): #Use shu-mkmeal <name> <wmin> <wmax> <smul>
        args = message.content.split(" ")
        mtr = 0
        args.pop(0)

        if adId in [role.id for role in message.author.roles] or tphId in [role.id for role in message.author.roles]:
            if len(args) == 4:
                if len(meals) > 0:
                    for i in meals:
                        if args[0] == i.name:
                            mtr = 1
                    if mtr == 0:
                        meGen(args[0], int(args[1]), int(args[2]), float(args[3]))
                        picS()
                        await client.send_message(message.channel, "If you didn't mistype anything this meal was created")
                    elif mtr == 1:
                        await client.send_message(message.channel, "There's already a meal with this name")
                if len(meals) == 0:
                    meGen(args[0], int(args[1]), int(args[2]), int(args[3]))
                    await client.send_message(message.channel, "If you didn't mistype anything this meal was created")

            if len(args) != 4:
                await client.send_message(message.channel, "Wrongly typed command,")
                await client.send_message(message.channel, "use 'shu-mkmeal <name_no_spaces> <min wg> <max wg> <size multiplier>'")
        elif adId not in [role.id for role in message.author.roles]:
            await client.send_message(message.channel, "You don't have the permisson to use this command, only moderators/admins can use it.")
    if message.content.lower().startswith('chu-meals'): #Use chu-meals
        mnl = []
        for i in meals:
            mnl.append(i.name)
        await client.send_message(message.channel, "Meals are %s" %(mnl))
    if message.content.lower().startswith('shu-rmeal'): #Use shu-rmeal <name> -y
        args = message.content.split(" ")

        if adId in [role.id for role in message.author.roles] or tphId in [role.id for role in message.author.roles]:
            if len(args) == 3:
                for i in meals:
                    if arg[1].lower() == i.name.lower():
                        meals.pop(meals.index(i))
                picS()
                await client.send_message(message.channel, "If there was a meal with that name, it is now removed.")
            elif len(args) !=3:
                await client.send_message(message.channel, "Wrongly typed command, use 'shu-rmeal <name> -y', WARNING: This command won't request confirmation before removal")
        elif adId not in [role.id for role in message.author.roles]:
            await client.send_message(message.channel, "You don't have the permisson to use this command, only moderators/admins can use it.")
    if message.content.lower().startswith('chu-help'): #Use chu-help
        m = message
        await sayT(m,"---*ChuBot Help*---")
        await sayT(m,"[+]To register, use:")
        await sayT(m,'```chu-register <weight in Kg> <waist size in centimeters>```')
        await sayT(m,"Your role as feedee/feeder will be automatically set. If you are just a feeder, don't put weight or size.")
        await sayT(m,"[+]To unregister, use: ```chu-unregister```")
        await sayT(m,"If you changed roles (ex.: feeder to both), you'll have to unregister and register again.")
        await sayT(m,"[+]To feed someone, use: ```-feed <@user> <meal_name>```")
        await sayT(m,"[+]To show meals: ```chu-meals```")
        await sayT(m,"[+]Show your status with: ```-me```")
        await sayT(m,"[+]To see everyone's status in a rank: ```chu-rank```")
        await sayT(m,"[+]To back to your original size/weight ```chu-reset```")
        await sayT(m,"Any profiles made before 1.2 won't be able to reset")
        await sayT(m,"[ADM]Set someone's weight/size with: ```shu-sw <@user> <weight> <size> <feedings>```")
        await sayT(m,"[ADM]Remove someone from the bot with: ```shu-rm <@user> -y```")
        await sayT(m,"[ADM]Create custom meal with:```shu-mkmeal <name_noSpace> <min. wg> <max. wg> <size multiplier>```")
        await sayT(m,"On the size multiplier, I recommend something between 1.0 and 2.0, ex.: 1.3")
        await sayT(m,"[ADM]To remove a meal: ```shu-rmeal <name> -y```")
        await sayT(m,"[!] Admin commands won't require comfirmation, so, beware.")
        await sayT(m,"[-]Show this dialoge")
        await sayT(m,"```chu-help```")
        await sayT(m, "[-]Show version: ```chu-ver```")
        await sayT(m,"-----------------")
    if message.content.lower().startswith('chu-ver'):
        m = message
        await sayT(m, "**chuBot** v*%s* | By *Waces Ferpit*" %(bover))
        picS()
    if message.content.lower().startswith("chu-reset"): #use: chu-reset
        orvt = message.author.id
        urt = True
        await client.send_message(message.channel, "Are you sure %s? I'tll reset your weight and size, say '-ryes' to reset or say '-rno' to cancel" %(message.author.mention))
    if message.content.lower().startswith("-ryes") and urt == True:
        vgg = "a"
        trs = 5
        if message.author.id == orvt:
            for i in profis:
                if orvt in i.uid:
                    i.wg = i.dwg
                    i.sz = i.dsz
                    vgg = i
                    trs += 5
            if trs > 5:
                picS()
                await client.send_message(message.channel, "%s reset their weight and size to %skg %scm" %(message.author.name,vgg.wg,vgg.sz))
                urt = False
                orvt = "a"
                trs = 5
            elif trs < 9:
                await client.send_message(message.channel, "Your profile wasn't reset, are you sure you requested removal?")
                trs = 5
                urt = False
                orvt = "a"
        elif message.author.id != orvt:
            await client.send_message(message.channel, "You %s don't seem to be the one that requested to reset, please don't mess with other people's profiles" %(message.author.mention))
    if message.content.lower().startswith("-rno") and urg == True:
        if message.author.id == orvt:
            orvt = "a"
            urt = False
            await client.send_message(message.channel, "You cancelled the reset, %s!" %(message.author.mention))
        elif message.author.id != orvt:
            await client.send_message(message.channel, "You %s don't seem to be the one that requested to reset, please don't mess with other people's profiles" %(message.author.mention))


client.run(token)

#If you think this code sucks, well mee too :P
